from setuptools import setup

setup(
    name='ecor',
    version='0.3',
    description='Reproject ECOSTRESS products from swath to a user requested spatial reference system',
    url='https://gitlab.com/thermopolis/public/ecor',
    author='Nikos Alexandris',
    author_email='Nikos.Alexandris@ec.europa.eu',
    license='',
    packages=['ecor'],
    zip_safe=False,
    install_requires=[
        'Click',
        'numpy',
        'pyproj',
        'pyresample==1.14.0',
        'rasterio',
        'scipy',
    ],
    entry_points='''
        [console_scripts]
        ecor=ecor.cli:cli
    ''',
)
