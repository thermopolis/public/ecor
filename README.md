## `ecor`

`ecor` (read _**ECO**STRESS **r**eproject_) will reproject ECOSTRESS products
from their swath area in to other well know spatial reference systems.

>Jump straight to the [Install](#install) section
or read the following examples
to get a taste of what `ecor` can do

## Examples

### `ecor describe`
```python
ecor describe ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5
```
will return
```bash
/geo/projects/thermopolis.public/ecor/ecor_virtualenv/lib/python3.7/site-packages/rasterio/__init__.py:219: NotGeoreferencedWarning: Dataset has no geotransform set. The identity matrix may be returned.
  s = DatasetReader(path, driver=driver, sharing=sharing, **kwargs)
['Emis1',
 'Emis5_err',
 'EmisWB',
 'LST',
 'LST_err',
 'PWV',
 'QC',
 'Emis1_err',
 'Emis2',
 'Emis2_err',
 'Emis3',
 'Emis3_err',
 'Emis4',
 'Emis4_err',
 'Emis5']
```

Describing then one of the subdatasets
requires only adding
the name of the subdataset
in the end of the previous command.

```python
ecor describe ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5 LST
```
will return
```bash
/geo/projects/thermopolis.public/ecor/ecor_virtualenv/lib/python3.7/site-packages/rasterio/__init__.py:219: NotGeoreferencedWarning: Dataset has no geotransform set. The identity matrix may be returned.
  s = DatasetReader(path, driver=driver, sharing=sharing, **kwargs)
{'SDS_LST__FillValue': '0 ',
 'SDS_LST_add_offset': '0 ',
 'SDS_LST_coordsys': 'cartesian',
 'SDS_LST_err__FillValue': '0 ',
 'SDS_LST_err_add_offset': '0 ',
 'SDS_LST_err_coordsys': 'cartesian',
 'SDS_LST_err_format': 'scaled',
 'SDS_LST_err_long_name': 'Land Surface Temperature error',
 'SDS_LST_err_scale_factor': '0.04 ',
 'SDS_LST_err_units': 'K',
 'SDS_LST_err_valid_range': '1 255 ',
 'SDS_LST_format': 'scaled',
 'SDS_LST_long_name': 'Land Surface Temperature',
 'SDS_LST_scale_factor': '0.02 ',
 'SDS_LST_units': 'K',
 'SDS_LST_valid_range': '7500 65535 '}
```

### `ecor reproject`

```python
ecor reproject \
    ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5 \
    ECOSTRESS_L1B_GEO_05525_002_20190627T070809_0600_01.h5 \
    LST \
    -o lst.tif
```
will output the following reprojected land surface temperature map
(colorised under GRASS GIS though)

<figure>
    <img
        src="images/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01_LST_WGS84.png" height=300
        alt="LST subdataset from ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01 reprojected in WGS84"
    />
    <figcaption>
        LST subdataset from the ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01 product reprojected in WGS84
    </figcaption>
</figure>

## Install

`ecor` strives to become a proper Python package.
It is recommended to install it inside a virtual environment.

So, create a virtual environment
```
python -m venv ecor_virtualenv  # or `python3`
source ecor_virtualenv/bin/activate
```
and install `pip` + `ecor`
```
pip install -U pip
pip install git+https://gitlab.com/thermopolis/public/ecor
```
or if asked, add the `--user` option, i.e.
```
pip install -U pip --user
pip install git+https://gitlab.com/thermopolis/public/ecor --user
```

If you are behind a firewall,
and have not set a proxy for git/ssh,
here's a quick work-around:
```
wget https://gitlab.com/thermopolis/public/ecor/-/archive/master/ecor-master.tar.gz
tar xvf ecor-master.tar.gz
mv ecor-master.tar.gz /tmp  # or `rm -r` for the matter
mv ecor-master ecor
pip install ecor
```

Test...
```
ecor
```
should return some _usage_ message.


>**In case you would like to contribute**,
>install the module in editable mode --
>always preferrably inside a virtual environment:
>```
>pip install -e git+https://gitlab.com/thermopolis/public/ecor
>```

>This way,
>any modifications to the source code
>will be immediatedly available
>in the command line
>without the need to re-install the packae.

### Under development

- Output is GeoTiff UInt16: convert by default to Kelvin

## Python API

`ecor` features an API


```
ecor.describe_hdf5
ecor.reproject_ecostress_subdataset
ecor.reproject_ecostress_product
```

### Internal workflows

The command line interface is build in `cli.py` and consumes functions from
`api.py`. The API returns data structures or writes (reprojected) output files.

Specifically, the reprojection process is designed as follows:

- Input subdataset(s):
  - if one subdataset:
    - reproject it
    - write it out if requested
  - if multiple subdatasets:
    - compute distance matrices once
    - for each subdataset
      - reproject subdataset by reusing computed distance matrices
      - write out if requested  # all or nothing, no selective write-out!

[**Update API Documentation**]


## To Do

- `ecor reproject` without `-o` should print out a summary of the reprojected data set?
- Add option to output csv with `x, y, z, value, timestamp`
- Colorise LST and Emissivity maps?
- Suppress warnings
- Print out clean, i.e. without brackets and single quotes
- Save tags, see [rasterio's tags topic](https://rasterio.readthedocs.io/en/latest/topics/tags.html)
- Alternative resamplings? Examples?

## Under the hood

- rasterio
- pyresample
