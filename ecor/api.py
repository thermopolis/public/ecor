from .constants import AREA_ID
from .constants import CELSIUS
from .constants import DESCRIPTION
from .constants import GEOGRAPHIC_AREA_ATTRIBUTES
from .constants import GEOGRAPHIC_DESCRIPTION
from .constants import GEOGRAPHIC_OGCWKT
from .constants import GEOGRAPHIC_PROJ_ID
from .constants import KELVIN
from .constants import LATITUDE
from .constants import LONGITUDE
from .constants import OGCWKT
from .constants import OUTPUT_FORMAT_DRIVER
from .constants import OUTPUT_NODATA
from .constants import OUTPUT_COMPRESSOR
from .constants import OUTPUT_EXTENSION
from .constants import PROJ_ID
from .constants import SINUSOIDAL_AREA_ATTRIBUTES
from .constants import SINUSOIDAL_DESCRIPTION
from .constants import SINUSOIDAL_OGCWKT
from .constants import SINUSOIDAL_PROJ_ID
from .describe import list_hdf5_subdatasets
from .utilities import count_hdf5_subdatasets
from .utilities import read_hdf5_subdataset
from .metadata import retrieve_hdf5_metadata
from .metadata import retrieve_subdataset_metadata
from .area import define_target_area
from .reproject import swath_to_projection
from .scale import scale_subdatasets
from .write import write_to_output

def describe_hdf5(
        hdf5,
        subdatasets,
        output,
        complete_names,
        quiet,
    ):
    """
    This functions either lists all subdatasets encapsulated inside the input
    'hdf5' file or the metadata attributes for the requested subdataset(s)

    Parameters
    ----------
    hdf5:

    subdataset:

    output:

    complete_names:
        A boolean flag whether to retrieve complete HDF5 subdataset names

    quiet:
        A boolean flag to adjust verbosity

    Returns
    -------
    A list of all subdatasets in the input 'hdf5' file
    or a list of metadata attributes for the requested subdataset(s)
    """
    if count_hdf5_subdatasets(hdf5) == 1:
        metadata = retrieve_hdf5_metadata(hdf5)
        return metadata

    else:
        if not subdatasets or subdatasets[0].lower() == 'all':
            return list_hdf5_subdatasets(hdf5, complete_names)

        else:
            subdatasets_metadata = dict()
            for subdataset in subdatasets:
                subdataset_metadata = retrieve_subdataset_metadata(subdataset, hdf5)
                subdatasets_metadata[subdataset] = subdataset_metadata
            # subdatasets_metadata = [f"{k}: {v}" for k,v in subdatasets_metadata.items()]
            return subdatasets_metadata

def reproject_ecostress_product(
        hdf5,
        subdatasets,
        geolocation,
        target_proj_id,
        pixel_size,
        unsquared_pixels,
        resampler,
        neighbours,
        radius,
        output,
        raw_output,
        celsius,
        quality,
        nprocs,
        quiet,
    ):
    """
    hdf5:
        An ECOSTRESS product in form of an HDF5 file

    subdatasets:

    geolocation:
        A corresponding ECOSTRESS geolocation product in form of an HDF5 file

    target_proj_id:
        A projection identifier string

    pixel_size:
        Pixel size in meters to compute the number of rows and columns, as well
        as the pixel shape of the reprojected product

    resampler:
        The resampling algorithm used the projection process

    radius:

    neighbours:

    output:
        The reprojected output filename

    units:
        Temprature units for the LST subdataset

    nprocs:
        Number or parallel processes for some processes

    unsquared_pixels:
        A boolean flag whether to square the pixel shape of the output
        reprojected products

    quiet:
        A boolean flag to adjust verbosity

    Returns
    -------
    reprojected_subdatasets:

    """
    if target_proj_id == GEOGRAPHIC_PROJ_ID:
        target_area_attributes = GEOGRAPHIC_AREA_ATTRIBUTES

    if target_proj_id == SINUSOIDAL_PROJ_ID:
        target_area_attributes = SINUSOIDAL_AREA_ATTRIBUTES

    longitude = read_hdf5_subdataset(LONGITUDE, geolocation)
    latitude = read_hdf5_subdataset(LATITUDE, geolocation)

    target_area_definition = define_target_area(
            latitude=latitude,
            longitude=longitude,
            area_id=target_area_attributes[AREA_ID],  # area_attributes is a dictionary
            description=target_area_attributes[DESCRIPTION],
            proj_id=target_area_attributes[PROJ_ID],
            projection_ogc_wkt=target_area_attributes[OGCWKT],
            source_projection_ogc_wkt=GEOGRAPHIC_OGCWKT,  # practically always the same
            pixel_size=pixel_size,
            unsquared_pixels=unsquared_pixels,
            nprocs=nprocs,
            quiet=quiet,
    )

    if count_hdf5_subdatasets(hdf5) == 1:
        subdatasets = [hdf5]  # treat as subdataset

    if not subdatasets or subdatasets[0].lower() == 'all':
        subdatasets = list_hdf5_subdatasets(hdf5, complete_names=False)

    reprojected_subdatasets = swath_to_projection(
            hdf5=hdf5,
            swath_datasets=subdatasets,
            longitude=longitude,
            latitude=latitude,
            target_definition=target_area_definition,
            radius=radius,
            neighbours=neighbours,
            epsilon=0,
            reduce_data=True,
            resample_type=resampler,
            weight_funcs=None,
            fill_value=0,  # Fill value defined in input subdataset
            with_uncert=False,
            segments=None,
            nprocs=nprocs,
            quiet=quiet,
    )

    if not raw_output:
        reprojected_subdatasets = scale_subdatasets(
                reprojected_subdatasets,
                hdf5,
        )

    if not output:
        return reprojected_subdatasets
    else:
        for reprojected_subdataset in reprojected_subdatasets:
            output_data = reprojected_subdatasets[reprojected_subdataset]  # for clarity
            # output_fill_value = retrieve_subdataset_fill_value(
            #         subdataset=reprojected_subdataset,
            #         hdf5=hdf5,
            # )
            # Above, does not handle subdatasets without a 'FillValue', i.e. QC

            if len(reprojected_subdatasets) == 1:
                output_filename = output  # FIXME - cli option == name + extension?

            else:
                prefix = output
                filename = reprojected_subdataset
                suffix = ''
                extension = OUTPUT_EXTENSION
                output_filename = f"{prefix}_{filename}{suffix}.{extension}"

            write_to_output(
                dataset=output_data,
                target_definition=target_area_definition,
                output_filename=output_filename,
                driver=OUTPUT_FORMAT_DRIVER,  # FIXME - add cli option?
                # nodata=output_fill_value,  # FIXME - Check sanity!
                nodata=OUTPUT_NODATA,  # FIXME - Check sanity!
                compress=OUTPUT_COMPRESSOR,  # FIXME - add cli option?
            )
