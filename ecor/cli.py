import click
import functools
from .constants import CELSIUS
from .constants import KELVIN
from .constants import ECOSTRESS_PIXEL_SIZE
from .constants import RESAMPLER
from .constants import NEAREST_NEIGHBOURS
from .constants import NEAREST_NEIGHBOUR_RADIUS
from .constants import LONGITUDE
from .constants import LATITUDE
from .constants import GEOGRAPHIC_PROJ_ID
from .constants import SINUSOIDAL_PROJ_ID
from .utilities import count_hdf5_subdatasets
from . import api
import pprint

@click.group()
def cli():
    pass

def common_options(f):
    options = [
        click.argument(
            'hdf5',
            type=click.Path(
                exists=True,
                dir_okay=False,
                )
        ),
        click.option(
            "-o",
            "--output",
            type=click.Path(
                exists=False,
                dir_okay=False,
                resolve_path=True,
                ),
            help="Output (GeoTiff) file name. Note, in case of multiple subdatasets, the given output will be used as file name prefix.",
        ),
        # click.option("-d",
        #              "--output-directory",
        #              type=click.Path(
        #                  exists=False,
        #                  dir_okay=True,
        #                  resolve_path=True),
        #              help="Output [CSV] file",
        #             ),
        click.option(
            "-q",
            "--quiet",
            is_flag=True,
            default=False,
            help="Suppress progress bars and messages",
        ),
    ]
    return functools.reduce(lambda x, opt: opt(x), options, f)

@cli.command('describe')
@click.option(
        "-c",
        "--complete-names",
        is_flag=True,
        default=False,
        help="List full HDF5 subdataset names",
)
@common_options
@click.argument(
        'subdataset',
        required=False,
        nargs=-1,
)
def describe(hdf5, subdataset, output, complete_names, quiet):
    """
    List HDF5 subdatasets
    """
    description = api.describe_hdf5(
            hdf5,
            subdataset,
            output,
            complete_names,
            quiet,
            )
    if not description:
        click.echo(click.style(
                               'It seems that the given HDF5 file does not contain any data!',
                               fg='red',
                  )
        )

    if count_hdf5_subdatasets(hdf5) == 1:
        print('Single dataset')
        for item, value in description.items():
            print(f'  {item}: {value}')

    else:
        if type(description) == list:
            print('Subdatasets')
            for element in sorted(description):
                print(f'  {element}', sep='\n')
        else:
            for subdataset, metadata in description.items():
                print(subdataset)
                for key, value in description[subdataset].items():
                    print(f"  {key} : {value}", sep='\n')


@cli.command('reproject')
@click.option(
        '-t',
        '--target-reference-system',
        is_flag=False,
        default=GEOGRAPHIC_PROJ_ID,
        help=f"Target spatial reference system '{GEOGRAPHIC_PROJ_ID}' or '{SINUSOIDAL_PROJ_ID}' [default: '{GEOGRAPHIC_PROJ_ID}']",
)
@common_options
@click.argument('geolocation')
@click.argument(
        'subdataset',
        required=False,
        nargs=-1,
)
@click.option(
        '-p',
        '--pixel-size',
        is_flag=False,
        default=ECOSTRESS_PIXEL_SIZE,
        required=False,
        help=f"Output pixel size [default: {ECOSTRESS_PIXEL_SIZE}m]"
)
@click.option(
        '-U',
        '--unsquared-pixels',
        is_flag=True,
        default=False,
        required=False,
        help="Do not square pixel shape in output grid",
)
@click.option(
        '-R',
        '--raw-output',
        is_flag=True,
        default=False,
        required=False,
        help="Do not apply scaling factor and offset values in the output data",
)
@click.option(
        '-C',
        '--celsius',
        is_flag=True,  # FIXME : Maybe True?
        default=False,
        required=True,
        help=f":: NOT IMPLEMENTED :: Temperature unit for LST output pixel values [default: {KELVIN} or {CELSIUS}]",
)
@click.option(
        '-Q',
        '--quality',
        is_flag=False,
        default=00,
        required=False,
        help=f":: NOT IMPLEMENTED :: Quality Control to filter...",
)
@click.option('-s',
        '--statistics',
        is_flag=True,
        default=False,
        help="Print statistics",
)
@click.option('-m',
        '--method',
        is_flag=False,
        default=RESAMPLER,
        required=False,
        help=f"Resampling method [default: {RESAMPLER}",
)
@click.option('-n',
        '--neighbours',
        is_flag=False,
        default=NEAREST_NEIGHBOURS,
        required=False,
        help=f"Number of neigbours to consider for each grid point [default: {NEAREST_NEIGHBOURS}]",
)
@click.option('-r',
        '--radius',
        is_flag=False,
        default=NEAREST_NEIGHBOUR_RADIUS,
        required=False,
        help=f"Cut off distance in meters [default: {NEAREST_NEIGHBOUR_RADIUS}]",
)
@click.option(
        '-c',
        '--cores',
        is_flag=False,
        default=1,
        help="Number of processor cores to use for certain calculations [default: 1]",
)
def reproject(
        hdf5,
        geolocation,
        subdataset,
        target_reference_system,
        pixel_size,
        unsquared_pixels,
        raw_output,
        celsius,
        quality,
        statistics,
        method,
        neighbours,
        radius,
        output,
        cores,
        quiet,
    ):
    """
    Reproject ECOSTRESS products to ...
    """
    target_reference_system = target_reference_system.lower() # thus accept capitalised strings
    reprojected_ecostress_product = api.reproject_ecostress_product(
            hdf5=hdf5,
            subdatasets=subdataset,
            geolocation=geolocation,
            target_proj_id=target_reference_system,
            pixel_size=pixel_size,
            unsquared_pixels=unsquared_pixels,
            resampler=method,
            neighbours=neighbours,
            radius=radius,
            output=output,
            raw_output=raw_output,
            celsius=celsius,
            quality=quality,
            nprocs=cores,
            quiet=quiet,
    )
    if statistics:
        from scipy import stats
        for item in subdataset:
            statistics = stats.describe(reprojected_ecostress_product[item])
            click.echo(pprint.pprint(statistics))

    else:
        click.echo(pprint.pprint(reprojected_ecostress_product))
