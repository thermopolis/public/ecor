from .utilities import count_hdf5_subdatasets
from .metadata import retrieve_hdf5_metadata_value
from .metadata import retrieve_subdataset_metadata_value
from .constants import METADATA_SCALE_FACTOR_IDENTIFIER
from .constants import METADATA_ADD_OFFSET_IDENTIFIER
from .constants import METADATA_FORMAT_IDENTIFIER
from .constants import SDS_FORMAT_SCALED

def scale_subdatasets(subdatasets, hdf5):
    """
    Parameters
    ----------
    subdatasets:
        A dictionary of (already reprojected?) ECOSTRESS subdatasets

    Returns
    -------
    A dictionary of the input subdatasets lineary transformed 

    """
    scaled_subdatasets = dict()
    for subdataset in subdatasets:
        if count_hdf5_subdatasets(hdf5) == 1:
            subdataset_format = retrieve_hdf5_metadata_value(
                                METADATA_FORMAT_IDENTIFIER,
                                hdf5,
            )
        else:
            subdataset_format = retrieve_subdataset_metadata_value(
                                METADATA_FORMAT_IDENTIFIER,
                                subdataset,
                                hdf5,
            )

        # Maybe the following will fail in case of HDF5 files with a single and
        # scaled data set  == No multiple subdatasets
        if subdataset_format[0].lower() == SDS_FORMAT_SCALED:
            subdataset_scaling_factor = retrieve_subdataset_metadata_value(
                                            METADATA_SCALE_FACTOR_IDENTIFIER,
                                            subdataset,
                                            hdf5,
                                        )
            scaling_factor = float(subdataset_scaling_factor[0])
            subdataset_offset = retrieve_subdataset_metadata_value(
                                    METADATA_ADD_OFFSET_IDENTIFIER,
                                    subdataset,
                                    hdf5,
                                )
            offset = float(subdataset_offset[0])

        else:
            scaling_factor = 1
            offset = 0

        data = subdatasets[subdataset]
        scaled_data = data * scaling_factor + offset
        scaled_subdatasets[subdataset] = scaled_data

    return scaled_subdatasets
