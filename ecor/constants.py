CELSIUS = 'Celsius'
ECOSTRESS_PIXEL_SIZE = 70  # nominal pixel size
KELVIN = 'Kelvin'
LATITUDE = 'latitude'
LONGITUDE = 'longitude'
METADATA_LONG_NAME_IDENTIFIER = 'long_name'
METADATA_ADD_OFFSET_IDENTIFIER = 'add_offset'
METADATA_FORMAT_IDENTIFIER = 'format'
METADATA_SCALE_FACTOR_IDENTIFIER = 'scale_factor'
METADATA_FILL_VALUE_IDENTIFIER = 'FillValue'
NEAREST_NEIGHBOURS = 1
NEAREST_NEIGHBOUR_RADIUS = 210
OUTPUT_COMPRESSOR = 'lzw'  # 'COMPRESS=DEFLATE'?
OUTPUT_EXTENSION = 'tif'
OUTPUT_FORMAT_DRIVER = 'GTiff'
OUTPUT_NODATA = 0  # All subdatasets in LSTE products feature a FillValue = 0
RESAMPLER = 'nn'
SDS_FORMAT_SCALED = 'scaled'
SDS_FORMAT_UNSCALED = 'unscaled'

# Projection definitions
AREA_ID = 'area_id'
DESCRIPTION = 'description'
GEOGRAPHIC_AREA_ID = 'EPSG:4326'
GEOGRAPHIC_DESCRIPTION = 'Geographic'
GEOGRAPHIC_OGCWKT = 'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]]'
GEOGRAPHIC_PROJ = '+proj=longlat +datum=WGS84 +no_defs'
GEOGRAPHIC_PROJ_ID = 'geographic'
OGCWKT = 'ogcwkt'
PROJECTION = 'projection'
PROJ_ID = 'proj_id'
GEOGRAPHIC_AREA_ATTRIBUTES = {
        'area_id': GEOGRAPHIC_AREA_ID,
        'description': GEOGRAPHIC_DESCRIPTION,
        'proj_id': GEOGRAPHIC_PROJ_ID,
        'projection': GEOGRAPHIC_PROJ,
        'ogcwkt': GEOGRAPHIC_OGCWKT,
        }
SINUSOIDAL_AREA_ID = 'ESRI:54008'
SINUSOIDAL_DESCRIPTION = 'Sinusoidal'
SINUSOIDAL_OGCWKT = 'PROJCS["MODIS Sinusoidal",GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],PROJECTION["Sinusoidal"],PARAMETER["false_easting",0.0],PARAMETER["false_northing",0.0],PARAMETER["central_meridian",0.0],PARAMETER["semi_major",6371007.181],PARAMETER["semi_minor",6371007.181],UNIT["m",1.0],AUTHORITY["SR-ORG","6974"]]'
SINUSOIDAL_PROJ_ID = 'sinusoidal'
SINUSOIDAL_RADIUS = 6371007.181
SINUSOIDAL_PROJ = f"+proj=sinu +R={SINUSOIDAL_RADIUS} +units=m"
SINUSOIDAL_AREA_ATTRIBUTES = {
        'area_id' : SINUSOIDAL_AREA_ID,
        'description' : SINUSOIDAL_DESCRIPTION,
        'proj_id' : SINUSOIDAL_PROJ_ID,
        'projection' : SINUSOIDAL_PROJ,
        'ogcwkt': SINUSOIDAL_OGCWKT,
        }
