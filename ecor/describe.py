import rasterio

def list_hdf5_subdatasets(hdf5, complete_names):
    """
    Parameters
    ----------
    hdf5:
        An HDF5 file, presumably an ECOSTRESS product

    complete_names:
        A boolean flag whether to retrieve the complete SDS or the short name
        for each subdataset

    quiet:
        A boolean flag for verbosity

    Returns
    -------


    Examples
    --------
    >>> from ecor import utilities
    >>> utilities.list_hdf5_subdatasets('ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5', complete_names=False, quiet=False)
    ['Emis1',
    'Emis5_err',
    'EmisWB',
    'LST',
    'LST_err',
    'PWV',
    'QC',
    'Emis1_err',
    'Emis2',
    'Emis2_err',
    'Emis3',
    'Emis3_err',
    'Emis4',
    'Emis4_err',
    'Emis5']

    Another example:
    >>> utilities.list_hdf5_subdatasets('ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5', complete_names=True, quiet=False)
    ['HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis1',
     'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis5_err',
     'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/EmisWB',
     'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/LST',
     'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/LST_err',
     'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/PWV',
     'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/QC',
     'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis1_err',
     'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis2',
     'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis2_err',
     'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis3',
     'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis3_err',
     'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis4',
     'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis4_err',
     'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis5']
    """
    if not complete_names:
        name = lambda subdataset: subdataset.rsplit('/')[-1]
    else:
        name = lambda subdataset: subdataset

    list_subdatasets = lambda hdf5: rasterio.open(hdf5).subdatasets
    subdatasets = list_subdatasets(hdf5)
    subdatasets = [
                    name(subdataset)
                    for subdataset
                    in subdatasets
                  ]
    return subdatasets


# FIXME : maybe use describe_hdf5_subdataset(s)() to build the dictionary?
def build_subdatasets_dictionary(hdf5):
    """
    Parameters
    ----------
    hdf5:
        An HDF5 file

    Returns
    -------
    subdatasets:
        A python dictionary listing the subdatasets in the input 'hdf5'

    Examples
    --------
    >>> utilities.build_subdatasets_dictionary('ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5')
    {'Emis1': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis1',
    'Emis5_err': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis5_err',
    'EmisWB': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/EmisWB',
    'LST': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/LST',
    'LST_err': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/LST_err',
    'PWV': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/PWV',
    'QC': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/QC',
    'Emis1_err': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis1_err',
    'Emis2': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis2',
    'Emis2_err': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis2_err',
    'Emis3': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis3',
    'Emis3_err': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis3_err',
    'Emis4': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis4',
    'Emis4_err': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis4_err',
    'Emis5': 'HDF5:tests/input/ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5://SDS/Emis5'}
    """
    subdatasets = {}
    name = lambda subdatasets: subdataset.rsplit('/')[-1]
    list_subdatasets = lambda hdf5: rasterio.open(hdf5).subdatasets
    for subdataset in list_subdatasets(hdf5):
        subdatasets[name(subdataset)] = subdataset
    return subdatasets

def retrieve_subdataset_complete_name(shortname, hdf5):
    """
    Parameters
    ----------
    subdataset:
        Subdataset whose complete [GDAL] name to retrieve

    hdf5:
        Input hdf5 file

    Returns
    -------
    The complete name of the requested subdataset
    """
    subdatasets = build_subdatasets_dictionary(hdf5)
    return subdatasets[shortname]

# FIXME - Is this function required? It segfaults while trying interactively
# def read_gdal_subdataset_band(subdataset_shortname, hdf5):
#     """
#     """
#     subdataset_longname = retrieve_subdataset_complete_name(subdataset_shortname, hdf5)
#     gdal_hdataset = gdal.Open(subdataset_longname, gdal.GA_ReadOnly)
#     return gdal_hdataset.GetRasterBand(1)


