import warnings  # https://rasterio.groups.io/g/main/topic/silencing/70046907?p=,,,20,0,0,0::recentpostdate%2Fsticky,,,20,2,0,70046907
import rasterio
from .describe import retrieve_subdataset_complete_name


def read_hdf5_dataset(hdf5):
    """
    """
    warnings.filterwarnings("ignore", category=rasterio.errors.NotGeoreferencedWarning)
    return rasterio.open(hdf5).read(1,)

def count_hdf5_subdatasets(hdf5):
    """
    """
    warnings.filterwarnings("ignore", category=rasterio.errors.NotGeoreferencedWarning)
    return rasterio.open(hdf5).count

def read_hdf5_subdataset(subdataset, hdf5):
    """
    Parameters
    ----------
    subdataset:
        Short name of subdataset in an HDF5 file, presumable an ECOSTRESS product

    Returns
    -------

    Example
    -------
    >>> input_hdf5 = "ECOSTRESS_L1B_GEO_05525_002_20190627T070809_0600_01.h5"
    >>> read_hdf5_subdataset('longitude', input_hdf5)
    'HDF5:ECOSTRESS_L1B_GEO_05525_002_20190627T070809_0600_01.h5://Geolocation/longitude'
    """
    # subdataset = lambda wanted: next(subdataset for subdataset in
    #                     geolocation_hdf5.subdatasets if wanted in subdataset)
    # match = (subdataset for subdataset in subdatasets if request in subdataset)
    warnings.filterwarnings("ignore", category=rasterio.errors.NotGeoreferencedWarning)
    complete_name = retrieve_subdataset_complete_name(subdataset, hdf5)
    return rasterio.open(complete_name).read(1,)

def read_hdf5_subdatasets(subdatasets, hdf5):
    """
    Parameters
    ----------
    subdatasets:
        List of subdatasets

    hdf5:
        An HDF5 file, presumably an ECOSTRESS product

    Returns
    -------
    hdf5_subdatasets:
        A list with the requested subdatasets in form of numpy arrays

    Examples
    --------
    >>> from ecor import utilities
    >>> utilities.read_hdf5_subdatasets(['LST'], 'ECOSTRESS_L2_LSTE_05525_002_20190627T070809_0600_01.h5')
    [array([[0, 0, 0, ..., 0, 0, 0],
            [0, 0, 0, ..., 0, 0, 0],
            [0, 0, 0, ..., 0, 0, 0],
            ...,
            [0, 0, 0, ..., 0, 0, 0],
            [0, 0, 0, ..., 0, 0, 0],
            [0, 0, 0, ..., 0, 0, 0]], dtype=uint16)]
    """
    warnings.filterwarnings("ignore", category=rasterio.errors.NotGeoreferencedWarning)
    hdf5_subdatasets = []
    for subdataset in subdatasets:
        hdf5_subdatasets.append(read_hdf5_subdataset(subdataset, hdf5))
    return hdf5_subdatasets
