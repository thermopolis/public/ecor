from .constants import GEOGRAPHIC_AREA_ID
from .constants import GEOGRAPHIC_DESCRIPTION
from .constants import GEOGRAPHIC_PROJ_ID
from .constants import GEOGRAPHIC_PROJ
from .constants import OUTPUT_FORMAT_DRIVER
from .constants import OUTPUT_COMPRESSOR
from .constants import OUTPUT_NODATA
from .utilities import read_hdf5_subdataset
import rasterio

def write_to_output(
        dataset,
        target_definition,
        output_filename,
        driver=OUTPUT_FORMAT_DRIVER,
        nodata=OUTPUT_NODATA,
        compress=OUTPUT_COMPRESSOR,
    ):
    """
    Parameters
    ----------
    dataset:
        The reprojected 'dataset' to write out to a GeoTiff. The 'product' can
        contain one, multiple or all of the original HDF5 subdatasets.

    output_filename:
        Self-eplainable, the output's filename

    driver:
        The output format driver

    nodata:
        The value to use for No Data

    compress:
        The compressor algorithm with which to compress the output file
        'output_filename'

    Returns
    -------
        This function writes the input 'dataset' in to the 'output_filename'
        file

    """
    height, width = dataset.data.shape  # rows, columns -- confirm!
    pixel_size = target_definition.pixel_size_x  # are pixels squared?
    upper_left_x = target_definition.area_extent[0]
    upper_left_y = target_definition.area_extent[3]
    # Transformation parameters
    # affine.Affine.from_gdal():
        # convert GDAL GeoTransform, a sequences of 6 numbers where:
        # c, a, b, f, d, e: 6 floats ordered by GDAL
        # 1st c: upper_left_x  # x offset
        # 2nd a: pixel_size_x
        # 3rd b: 
        # 4th f: upper_left_y  # y offset
        # 5th d:
        # 6th e: pixel_size_y
    transformation_parameters = rasterio.Affine.from_gdal(
            c = upper_left_x,
            a = pixel_size,
            b = 0.,
            f = upper_left_y,
            d = 0.,
            e = -pixel_size,
            )
    with rasterio.Env():
        with rasterio.open(
                output_filename,
                mode='w',
                driver=driver,
                height=height,
                width=width,
                count=1,  # - number of bands?
                dtype=dataset.dtype,
                crs=target_definition.crs.srs,
                transform=transformation_parameters,
                nodata=nodata,
                compress=compress,
             ) as dst:
                dst.write(dataset, 1)
