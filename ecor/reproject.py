from .constants import RESAMPLER
from .constants import METADATA_LONG_NAME_IDENTIFIER
from pyresample import geometry
from pyresample import kd_tree
from .utilities import count_hdf5_subdatasets
from .utilities import read_hdf5_subdataset
from .utilities import read_hdf5_dataset
from .metadata import retrieve_hdf5_metadata_value
from .area import define_target_area

def reproject_datasets(
        hdf5,
        source_geo_def,
        datasets,
        target_geo_def,
        radius_of_influence,
        neighbours,
        epsilon,
        reduce_data,
        resample_type,
        output_shape,
        weight_funcs,
        fill_value,
        with_uncert,
        nprocs,
        segments,
    ):
    """
    """
    # Perform once, reuse for reprojecting multiple data sets
    (
    valid_source_indices,
    valid_target_indices,
    index_array,
    distances,
    ) = kd_tree.get_neighbour_info(
            source_geo_def=source_geo_def,  # geometry.AreaDefinition
            target_geo_def=target_geo_def,  # geometry.AreaDefinition
            radius_of_influence=radius_of_influence,
            neighbours=neighbours,
            epsilon=epsilon,
            reduce_data=reduce_data,
            nprocs=nprocs,
            segments=segments,
        )
    reprojected_datasets = dict()
    for dataset in datasets:
        # dataset is a name/ dictionary key
        swath_data = read_hdf5_subdataset(dataset, hdf5)
        reprojected_data =  kd_tree.get_sample_from_neighbour_info(
                                resample_type=resample_type,
                                output_shape=target_geo_def.shape,
                                data=swath_data,
                                valid_input_index=valid_source_indices,
                                valid_output_index=valid_target_indices,
                                index_array=index_array,
                                distance_array=None,  # 'distances' from get_neighbour_info()
                                weight_funcs=None,
                                fill_value = None,
                                with_uncert=False,
                            )
        reprojected_datasets[dataset] = reprojected_data
    return reprojected_datasets

def swath_to_projection(
        hdf5,
        swath_datasets,
        latitude,
        longitude,
        target_definition,
        radius=100,
        neighbours=1,
        epsilon=0,
        reduce_data=True,
        resample_type=RESAMPLER,
        weight_funcs=None,
        fill_value=None,
        with_uncert=False,
        segments=None,
        nprocs=1,
        quiet=False,
    ):
    """
    This function resamples and reproject the swath input 'data' sets to the
    requested 'target_definition' spatial reference system

    Parameters
    ----------
    hdf5:
        An ECOSTRESS product in form of an HDF5 file

    latitude:
       A raster with latitude values for each...

    longitude:
        A raster with longitude values for each pixel...

    nprocs:
        Number of processing cores to use

   target_definition:
       The definition of the target spatial reference system

    Returns
    -------
    The requested 'swath_datasets' from the input ECOSTRESS product 'hdf5'
    georeferenced in the 'target_definition' spatial reference system

    Examples
    --------
    ..
    """
    swath_definition = geometry.SwathDefinition(
            lons=longitude,
            lats=latitude,
            nprocs=nprocs,
    )

    reprojected_datasets = dict()
    number_of_swath_datasets = len(swath_datasets)

    if number_of_swath_datasets == 1:
        if count_hdf5_subdatasets(hdf5) == 1:
            subdataset = retrieve_hdf5_metadata_value(METADATA_LONG_NAME_IDENTIFIER, hdf5)[0]
            swath_data = read_hdf5_dataset(hdf5)  # single list element
        if count_hdf5_subdatasets(hdf5) == 0 and number_of_swath_datasets == 1:
            subdataset = swath_datasets[0]
            swath_data = read_hdf5_subdataset(subdataset, hdf5)  # single list element

        # if quality:
        #     swath_data = filter_qualities(swath_data, quality)

        reprojected_dataset = kd_tree.resample_nearest(
            source_geo_def=swath_definition,  # geometry.AreaDefinition
            data=swath_data,
            target_geo_def=target_definition,  # geometry.AreaDefinition
            radius_of_influence=radius,
            epsilon=epsilon,
            fill_value=fill_value,
            reduce_data=reduce_data,
            nprocs=nprocs,
            segments=segments,
        )
        reprojected_datasets[subdataset] = reprojected_dataset

    elif number_of_swath_datasets > 1:
        reprojected_datasets = reproject_datasets(
            hdf5=hdf5,
            source_geo_def=swath_definition,  # geometry.AreaDefinition
            datasets=swath_datasets,
            target_geo_def=target_definition,  # geometry.AreaDefinition
            radius_of_influence=radius,
            neighbours=neighbours,
            epsilon=epsilon,
            reduce_data=reduce_data,
            resample_type=resample_type,
            output_shape=target_definition.shape,
            weight_funcs=weight_funcs,
            fill_value=fill_value,
            with_uncert=with_uncert,
            nprocs=nprocs,
            segments=segments,
        )

    else:
        raise ValueError(f'The number of subdatasets is {number_of_swath_datasets}! Check conditions of the if/elif statements.')

    return reprojected_datasets
