from .constants import GEOGRAPHIC_AREA_ID
from .constants import GEOGRAPHIC_DESCRIPTION
from .constants import GEOGRAPHIC_PROJ_ID
from .constants import GEOGRAPHIC_PROJ
from .constants import GEOGRAPHIC_OGCWKT
from .constants import SINUSOIDAL_AREA_ID
from .constants import SINUSOIDAL_DESCRIPTION
from .constants import SINUSOIDAL_PROJ_ID
from .constants import SINUSOIDAL_PROJ
from .projection import define_equidistant_projection
from .projection import define_equidistant_transformer
from .projection import define_sinusoidal_projection
from .projection import define_transformer
from pyresample import geometry
import pprint

def select_target_projection(area_id):
    """
    """
    if area_id == GEOGRAPHIC_AREA_ID:
        return GEOGRAPHIC_AREA_ID

    elif area_id == SINUSOIDAL_AREA_ID:
        return SINUSOIDAL_AREA_ID
    else:
        raise ValueError(f"The given 'area_id' {area_id} does not match... ")

def define_geographic_extent(latitude, longitude):
    """
    Create and return a list of 'area_extent' row & column indices in
    the following order:
        (lower_left_x, lower_left_y, upper_right_x, upper_right_y)

    Parameters
    ----------
    longitude:

    latitude:

    transform:

    Returns
    -------
    A tuple with the lower left and upper right coordinates that define the
    extent of the area

    Examples
    --------
    >>> ..
    """
    lower_left_x = longitude.min()
    lower_left_y = latitude.min()
    upper_right_x = longitude.max()
    upper_right_y = latitude.max()
    return (lower_left_x, lower_left_y, upper_right_x, upper_right_y)

def define_target_extent(
        latitude,
        longitude,
        source_projection_ogc_wkt,  # OGC WKT
        target_projection_ogc_wkt,  # OGC WKT
    ):
    """
    """
    # transform from decimal degrees to target units
    if not target_projection_ogc_wkt == GEOGRAPHIC_OGCWKT:
        transformer = define_transformer(
                from_projection_ogc_wkt=source_projection_ogc_wkt,
                to_projection_ogc_wkt=target_projection_ogc_wkt,
        )
        # >>>> Order is (latitude, longitude) -- NOT (longitude, latitude) <<<<
        lower_left_x, lower_left_y = transformer.transform(
                latitude.min(),
                longitude.min(),
        )
        upper_right_x, upper_right_y = transformer.transform(
                latitude.max(),
                longitude.max(),
        )
        return (lower_left_x, lower_left_y, upper_right_x, upper_right_y)

    else:
        return define_geographic_extent(
                latitude=latitude,
                longitude=longitude,
                )

def compute_rows_and_columns(
        latitude,
        longitude,
        pixel_size,
    ):
    """
    The computation of the rows and columns for the output is performed in an
    azimuthal and equidistand plane.

    Note: Currently, reading a HDF5 subdataset with rasterio, does not retrieve
    and store the CRS metadata.
    """
    # transform from decimal degrees to meters
    transformer = define_equidistant_transformer(
            latitude=latitude,
            longitude=longitude,
    )
    # input order is: latitude, longitude
    lower_left_x, lower_left_y = transformer.transform(
            latitude.min(),
            longitude.min(),
    )
    upper_right_x, upper_right_y = transformer.transform(
            latitude.max(),
            longitude.max(),
    )
    width = int((upper_right_x - lower_left_x) // pixel_size)
    height = int((upper_right_y - lower_left_y) // pixel_size)
    return (width, height)

def refine_area(
        area_definition,
        nprocs
    ):
    """
    This function refines the 'area_definition' by forcing squared pixels whose
    size is the smallest among the estimated x and y pixel sizes during the
    first area definition by geometry.AreaDefinition().
    """
    west = area_definition.area_extent[0]
    south = area_definition.area_extent[1]
    east = area_definition.area_extent[2]
    north = area_definition.area_extent[3]
    pixel_size = min([area_definition.pixel_size_x, area_definition.pixel_size_y])
    width = int((east - west) // pixel_size)
    height = int((north - south) // pixel_size)
    area_definition = geometry.AreaDefinition(
           area_id=area_definition.area_id,
           description=area_definition.description,
           proj_id=area_definition.proj_id,
           projection=area_definition.proj_dict,  # or .proj_string
           width=width,
           height=height,
           area_extent=area_definition.area_extent,
           rotation=None,
           nprocs=nprocs,
    )
    return area_definition

def define_area(
        latitude,
        longitude,
        area_id,
        description,
        proj_id,
        projection_ogc_wkt,
        source_projection_ogc_wkt,
        extent,
        pixel_size,
        nprocs=1,
        unsquared_pixels=False,
        quiet=False,
    ):
    """
    This function defines an area using the pyresample library's class
    'AreaDefinition'

    Parameters
    ----------
    latitude:

    longitude:

    area_id:

    description:

    proj_id:

    projection:
        String or dictionary of Proj parameters

    source_area_id:

    extent:

    pixel_size:

    nprocs:

    unsquared_pixels:
        A boolean selector whether to refine the area definition (default mode)
        by squaring the pixels in the target spatial reference system.
        Note, unsquared_pixels = True, will _not_ refine the area definition.

    quiet:

    Returns
    -------
        The Area Definition 'area_definition' build using the `pyresample` library

    """
    width, height = compute_rows_and_columns(
            latitude=latitude,
            longitude=longitude,
            pixel_size=pixel_size,
            )
    area_definition = geometry.AreaDefinition(
            area_id=area_id,  # area identifier
            description=description,  # human readable description
            proj_id=proj_id,
            projection=projection_ogc_wkt,  # string or dictionary
            width=width,
            height=height,
            area_extent=extent,
            rotation=None,
            nprocs=nprocs,
    )
    if not unsquared_pixels:  # if unsquared_pixels, do _not_ perform refinement
        area_definition = refine_area(
                area_definition=area_definition,
                nprocs=nprocs,
                )
    return area_definition

def define_target_area(
        latitude,
        longitude,
        area_id,
        description,
        proj_id,
        projection_ogc_wkt,
        source_projection_ogc_wkt,
        pixel_size,
        unsquared_pixels=False,
        nprocs=1,
        quiet=False,
    ):
    """

    Returns
    -------
        An pyresample.geometry.AreaDefinition object
    """
    target_extent = define_target_extent(
            latitude=latitude,
            longitude=longitude,
            source_projection_ogc_wkt=source_projection_ogc_wkt,
            target_projection_ogc_wkt=projection_ogc_wkt,
            )
    area = define_area(
            latitude=latitude,
            longitude=longitude,
            area_id=area_id,
            description=description,
            proj_id=proj_id,
            projection_ogc_wkt=projection_ogc_wkt,
            source_projection_ogc_wkt=source_projection_ogc_wkt,
            extent=target_extent,
            pixel_size=pixel_size,
            nprocs=nprocs,
            unsquared_pixels=unsquared_pixels,
            quiet=quiet,
    )
    return area
