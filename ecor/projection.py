from .constants import GEOGRAPHIC_AREA_ID
from .constants import GEOGRAPHIC_DESCRIPTION
from .constants import GEOGRAPHIC_PROJ_ID
from .constants import GEOGRAPHIC_PROJ
from .constants import GEOGRAPHIC_OGCWKT
import pyproj
from pyresample import geometry
import pprint

def define_equidistant_projection(latitude, longitude):
    """
    The axis order here follows what
    pyproj.Transformer expects as an input, which is (y, x) or else (latitude,
    longitude)
    """
    # middle_row = latitude.shape[0] // 2 #- 1
    # middle_column = latitude.shape[1] // 2 #- 1
    # center_longitude = longitude[middle_row, middle_column]
    # center_latitude = latitude[middle_row, middle_column]
    center_longitude = (longitude.min() + longitude.max()) / 2
    center_latitude = (latitude.min() + latitude.max()) / 2
    equidistand_projection = lambda origin_latitude, origin_longitude: \
            f"+proj=aeqd \
            +lat_0={origin_latitude} +lon_0={origin_longitude} \
            +x_0=0 +y_0=0 \
            +ellps=WGS84 +datum=WGS84 \
            +units=m no_defs"
    to_projection = equidistand_projection(
            origin_latitude=center_latitude,
            origin_longitude=center_longitude,
    )
    return to_projection

def define_sinusoidal_projection(latitude, longitude):
    """
    """
    # middle_row = latitude.shape[0] // 2 #- 1
    # middle_column = latitude.shape[1] // 2 #- 1
    # center_longitude = longitude[middle_row, middle_column]
    # center_latitude = latitude[middle_row, middle_column]
    center_longitude = (longitude.min() + longitude.max()) / 2
    center_latitude = (latitude.min() + latitude.max()) / 2
    equidistand_projection = lambda origin_latitude, origin_longitude: \
            f"+proj=sinu +R={6371007.181} +units=m"
    to_projection = equidistand_projection(
            origin_latitude=center_latitude,
            origin_longitude=center_longitude,
    )
    return to_projection

def define_transformer(from_projection_ogc_wkt, to_projection_ogc_wkt):
    """
    This function creates a 'transformer' function that will translate pixel
    location coordinates from the 'from_projection' to the 'to_projection'
    spatial reference systems.

    Context

    What does this function serve?  Reprojecting a 'swath' product in to
    another reference system, requires to define the target area in a spatial
    context: what is the extent and spatial resolution of the target area as
    well as where these will be located.

    Among the required parameters to define the area in the target reference
    system are the number of rows and columns: rows index the 'y' axis or
    'latitude' or the height and columns index the 'x' axis or 'longitude' or
    the width of an (geographic) area.

    We compute the number of rows and columns in the target reference system
    based on the bounding box coordinates and the pixel resolution of a
    georeferenced product.

    For an ECOSTRESS product, the source for the bounding box coordinates is
    the corresponding geolocation product which is referenced in geographic
    coordinates (WGS84, units: degrees) -- this is (**practically always**)
    the 'from_projection'.

    Translating the _geolocation_ bounding box in to an azimuthal equidistand
    plane --this is the 'to_projection', units: meters-- and considering
    ECOSTRESS' nominal pixel resolution set at 70m, we compute the 'real'
    number of rows and columns of the product.

    See also:

        >>> from pyproj import Proj, transform
        >>> src = rasterio.open('example.tif')
        >>> transform(Proj(src.crs), Proj('+init=epsg:3857'), 101985.0, 2826915.0)
        (-8789636.707871985, 2938035.238323653)

    Also note that recent Proj documentation, recommends not to use `proj`
    strings.

    Parameters
    ----------
    from_projection:
        The source reference system

    to_projection:
        The target reference system

    Returns
    -------
        A 'transformer' function that will translate pixel coordinates from the
        'from_projection' to the 'to_projection' reference system.

    Examples
    --------
    >>> import pyproj
    >>> wgs84 = 'epsg:4326'
    >>> origin_latitude, origin_longitude = (50.55301162299592, 1.7901016658931073)
    >>> azimuthal_equidistand = f"+proj=aeqd +lon_0={origin_longitude} +lat_0={origin_latitude} +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m no_defs"
    >>> to_projection = "+proj=aeqd +lon_0={center_longitude} +lat_0={center_latitude} +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m no_defs"
    >>> transformer(define_transformer(from_projection=wgs84, to_projection=azimuthal_equidistand)
    >>> transformer.transform(center_latitude, center_longitude)
    (0.0, 0.0)

    Another example:
    >>> transformer.transform(48.3573819921769, 0.248785901884064)
    (-114244.49929218103, -243020.47654965325)

    Another example:
    >>> transformer.transform(52.72967076413214, 3.4747841200955074)
    (113827.29793701784, 243481.9988992372)
    """
    # return pyproj.Transformer.from_proj(
    return pyproj.Transformer.from_proj(
            proj_from=from_projection_ogc_wkt,
            proj_to=to_projection_ogc_wkt,
            always_xy=False,  # don't use traditional (longitude, latitude)
            )

def define_equidistant_transformer(latitude, longitude):
    """
    """
    equidistand_projection = define_equidistant_projection(
        latitude,
        longitude,
        )
    return pyproj.Transformer.from_proj(
            proj_from=GEOGRAPHIC_OGCWKT,  # GEOGRAPHIC_AREA_ID,
            proj_to=equidistand_projection,
            always_xy=False,  # don't use traditional (longitude, latitude)
            )
